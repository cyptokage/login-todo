import { Switch, Route } from "react-router";
import {
  HOMEPAGE_ROUTE,
  SIGNUP_ROUTE,
  LOGIN_ROUTE,
  TODO_ROUTE,
} from "./constant";
import HomePage from "../src/pages/Home";
import LoginPage from "../src/pages/Login";
import Todo from "../src/pages/Todo";
import SingupPage from "../src/pages/Signup";
import "./App.css";
import { history } from "./utils/history";

function App(props) {
  const StaticValue = "BJP_SUCKS";
  return (
    <>
      <header
        style={{
          margin: "1rem 0rem",
        }}
      >
        <a
          style={{
            cursor: "pointer",
          }}
          onClick={() => history.push("/")}
        >
          Home
        </a>
        <a
          style={{
            cursor: "pointer",
          }}
          onClick={() => history.push("/login")}
        >
          Login
        </a>
        <a
          style={{
            cursor: "pointer",
          }}
          onClick={() => history.push("/signup")}
        >
          Signup
        </a>
      </header>
      <Switch>
        <Route exact path={LOGIN_ROUTE} component={LoginPage} />
        <Route exact path={SIGNUP_ROUTE} component={SingupPage} />
        <Route exact path={TODO_ROUTE} component={Todo} />
        <Route exact path={HOMEPAGE_ROUTE} component={HomePage} />
      </Switch>
    </>
  );
}

export default App;
