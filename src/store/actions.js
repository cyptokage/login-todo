import {
  ADD_TODO_ITEM_SUCCESS,
  LOGIN_SUCCESS,
  REMOVE_TODO_ITEM_SUCCESS,
  SIGNUP_SUCCESS,
} from "./constant";

export const signuSuccess = (data) => ({
  type: SIGNUP_SUCCESS,
  payload: data,
});

export const loginSuccess = (data) => ({
  type: LOGIN_SUCCESS,
  payload: data,
});

export const addTodoListSuccess = (data) => ({
  type: ADD_TODO_ITEM_SUCCESS,
  payload: data,
});

export const removeTodoListSuccess = (data) => ({
  type: REMOVE_TODO_ITEM_SUCCESS,
  payload: data,
});
