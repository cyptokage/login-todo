import { createStore } from "redux";
import { initialState } from "./reducer";
import {
  SIGNUP_SUCCESS,
  LOGIN_SUCCESS,
  ADD_TODO_ITEM_SUCCESS,
  REMOVE_TODO_ITEM_SUCCESS,
} from "./constant";
import pullAt from "lodash/pullAt";

export const store = createStore((state = initialState, action) => {
  switch (action.type) {
    case SIGNUP_SUCCESS:
      const signupList = [...state.signupList, action.payload];
      const todoObject = state.todoObject;
      todoObject[action.payload.email] = [];
      return {
        ...state,
        signupList,
        todoObject,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        loginData: action.payload,
      };
    case ADD_TODO_ITEM_SUCCESS:
      const todoObject1 = state.todoObject;
      todoObject1[state.loginData] = [
        ...todoObject1[state.loginData],
        action.payload,
      ];
      return {
        ...state,
        todoObject: todoObject1,
      };
    case REMOVE_TODO_ITEM_SUCCESS:
      let cloned = { ...state };
      const todoList = cloned.todoObject[cloned.loginData];
      const updatedList = todoList.filter((item, id) => id !== action.payload);
      cloned.todoObject[cloned.loginData] = updatedList;
      return cloned;

    default:
      return state;
  }
});
