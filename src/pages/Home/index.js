import React from "react";
import { connect } from "react-redux";

class HomePage extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <ul>
          {this.props.signupList.map((item, id) => (
            <li>
              <div>{item.name}</div>
              <div>{item.age}</div>
              <div>{item.email}</div>
              <div>{item.address}</div>
            </li>
          ))}
        </ul>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    signupList: state.signupList,
  };
};

export default connect(mapStateToProps)(HomePage);
