import React from "react";
import { connect } from "react-redux";
import { addTodoListSuccess, removeTodoListSuccess } from "../../store/actions";

class ToDo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.addTodoItem = this.addTodoItem.bind(this);
    this.removeItem = this.removeItem.bind(this);
  }

  componentDidMount() {
    if (this.props.loginData.length === 0) {
      this.props.history.push("/");
    }
  }
  handleChange = (e) => {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  };

  addTodoItem = (e) => {
    e.preventDefault();
    this.props.addTodoListSuccess(this.state.data);
  };

  removeItem = (id) => this.props.removeTodoListSuccess(id);

  render() {
    return (
      <>
        <div>
          <span>
            <input
              name="data"
              value={this.state.data}
              onChange={this.handleChange}
            />
          </span>
          <span>
            <button onClick={this.addTodoItem}>+</button>
          </span>
        </div>
        <ul>
          {this.props.todoList.map((item, id) => (
            <li className={id} key={id}>
              <span>
                <input value={item} readOnly />
              </span>
              <span>
                <button onClick={(id) => this.removeItem(id)}>-</button>
              </span>
            </li>
          ))}
        </ul>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  const loginData = state.loginData; //email data
  const todoObject = state.todoObject; // object of arrays
  return {
    todoList: todoObject[loginData] || [],
    loginData,
  };
};

const mapDispatchToProps = (dispatch) => ({
  addTodoListSuccess: (data) => dispatch(addTodoListSuccess(data)),
  removeTodoListSuccess: (data) => dispatch(removeTodoListSuccess(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ToDo);
