import React from "react";
import { signuSuccess } from "../../store/actions";
import { connect } from "react-redux";

class SignUp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      age: "",
      address: "",
      email: "",
      password: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = (e) => {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.signuSuccess(this.state);
  };

  render() {
    return (
      <>
        <div>
          <form onSubmit={this.handleSubmit}>
            <div>
              <label>name</label>
              <input
                name="name"
                value={this.state.name}
                onChange={this.handleChange}
                required
              />
            </div>
            <div>
              <label>age</label>
              <input
                name="age"
                value={this.state.age}
                onChange={this.handleChange}
                required
              />
            </div>
            <div>
              <label>address</label>
              <input
                name="address"
                value={this.state.address}
                onChange={this.handleChange}
                required
              />
            </div>
            <div>
              <label>email</label>
              <input
                name="email"
                value={this.state.email}
                onChange={this.handleChange}
                required
              />
            </div>
            <div>
              <label>password</label>
              <input
                name="password"
                type="password"
                value={this.state.password}
                onChange={this.handleChange}
                required
              />
            </div>
            <button type="submit">signup</button>
          </form>
        </div>
      </>
    );
  }
}

const mapStateToProps = () => ({});

const mapDispatchToProps = (dispatch) => ({
  signuSuccess: (data) => dispatch(signuSuccess(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
