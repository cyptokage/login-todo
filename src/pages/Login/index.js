import React from "react";
import { loginSuccess } from "../../store/actions";
import { connect } from "react-redux";
import { TODO_ROUTE_BASE } from "../../constant";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange = (e) => {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    const verifyData = this.props.signupList.find(
      (item) =>
        item.email === this.state.email && item.password === this.state.password
    );
    if (!verifyData) {
      alert("User Not Found");
      return;
    }
    this.props.loginSuccess(this.state.email);
    this.props.history.push(`${TODO_ROUTE_BASE}/${this.state.email}`);
  };

  render() {
    return (
      <>
        <form onSubmit={this.handleSubmit}>
          <div>
            <label>email</label>
            <input
              name="email"
              value={this.state.email}
              onChange={this.handleChange}
              required
            />
          </div>
          <div>
            <label>password</label>
            <input
              name="password"
              type="password"
              value={this.state.password}
              onChange={this.handleChange}
              required
            />
          </div>
          <button type="submit">Login</button>
        </form>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    signupList: state.signupList,
  };
};

const mapDispatchToProps = (dispatch) => ({
  loginSuccess: (data) => dispatch(loginSuccess(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
