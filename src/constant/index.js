export const HOMEPAGE_ROUTE = '/';
export const SIGNUP_ROUTE = '/signup';
export const LOGIN_ROUTE = '/login';
export const TODO_ROUTE_BASE = '/todo';
export const TODO_ROUTE = `${TODO_ROUTE_BASE}/:email`;